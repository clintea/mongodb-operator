#!/bin/bash
kubectl delete -f deploy/crds/lab_v1_mongodb_cr.yaml
kubectl delete -f deploy/operator.yaml
kubectl delete -f deploy/role_binding.yaml
kubectl delete -f deploy/role.yaml
kubectl delete -f deploy/service_account.yaml
kubectl delete -f deploy/crds/lab_v1_mongodb_crd.yaml
kubectl delete -f deploy/olm-catalog/mongodb-operator/0.0.1/mongodb-operator.v0.0.1.clusterserviceversion.yaml
