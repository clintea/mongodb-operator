#!/bin/bash
kubectl create -f deploy/crds/lab_v1_mongodb_crd.yaml
operator-sdk --image-builder=buildah build registry.gitlab.com/clintea/mongodb-operator:v1
podman login -u${GITLAB_USER} -p${GITLAB_PASSWORD} registry.gitlab.com
podman push registry.gitlab.com/clintea/mongodb-operator:v1
kubectl create -f deploy/service_account.yaml
kubectl create -f deploy/role.yaml
kubectl create -f deploy/role_binding.yaml
kubectl create -f deploy/operator.yaml
#oc policy add-role-to-group system:image-puller system:serviceaccounts:dev --namespace=lab
oc apply -f deploy/olm-catalog/mongodb-operator/0.0.1/mongodb-operator.v0.0.1.clusterserviceversion.yaml
#kubectl apply -f deploy/crds/lab_v1_mongodb_cr.yaml
